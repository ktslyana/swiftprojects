//
//  ViewController.swift
//  BirthdayTracker
//
//  Created by Vitalii Todorovych on 3/15/19.
//  Copyright © 2019 Correnet. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications


class AddBirthdayViewController: UIViewController {
    
    @IBOutlet var firstNameTextField: UITextField!
    @IBOutlet var lustNameTextField: UITextField!
    @IBOutlet var birthdatePicker: UIDatePicker!
    
  

    override func viewDidLoad() {
        super.viewDidLoad()
        
        birthdatePicker.maximumDate = Date()
    }

    @IBAction func saveTaped(_sender:UIBarButtonItem){
        print("press save button")
    
    let firstName = firstNameTextField.text ?? ""
    let lustName = lustNameTextField.text ?? ""
    let birthdate = birthdatePicker.date
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let context = appDelegate.persistentContainer.viewContext
        
    let newBirthday = Birthday(context: context)
        newBirthday.firstName = firstName
        newBirthday.lustName = lustName
        newBirthday.birthdate = birthdate as NSDate?
        newBirthday.birthdayId = UUID().uuidString
        
        if let uniqueId = newBirthday.birthdayId {
            print("birthdayId:\(uniqueId)")
        }
        do {
            try context.save()
            let message = "birthday \(firstName) \(lustName)"
            let content = UNMutableNotificationContent()
            content.body = message
            content.sound = UNNotificationSound.default()
            var dateComponents = Calendar.current.dateComponents([.month,.day], from: birthdate)
            dateComponents.hour = 8
            let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
            
            if let identifier = newBirthday.birthdayId {
                let reqest = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
                let center = UNUserNotificationCenter.current()
                center.add(reqest,withCompletionHandler: nil)
            }
            
        }catch let error {
            print("\(error)")
        }
        

        dismiss(animated: true, completion: nil)
       
}
    @IBAction func cancelTaped(_sender: UIBarButtonItem) {
        dismiss(animated: true,completion: nil)
    }
}

