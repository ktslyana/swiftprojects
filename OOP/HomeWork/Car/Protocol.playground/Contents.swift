//: Playground - noun: a place where people can play

import UIKit

protocol Engins {
    var isOkay : Bool {get set}
    var error  : String? {get set}
    
}

protocol Cars {

  
    var isStarted : Bool {get set}

    func start() -> Bool

    func run() -> Bool

}

class Engin : Engins {
    var isOkay : Bool = true
    var error  : String? = nil
}


class Car : Cars {
    
    enum Side {
        case right, left, up, down
    }
    var isStarted : Bool = false
    private let engin = Engin()
    
    func start() -> Bool {
        if (isStarted) {
            return true
        }
        if !engin.isOkay {
            if let error = engin.error {
                print(error)
            }
            return false
        }
        isStarted = true
        return true
    }

    
    func run() -> Bool {
        if !isStarted {
            return false
        }
        return true
    }
    
    func runBack() -> Bool {
       if !isStarted {
           return false
        }
        
        return true
    }
    
    func turn(side: Side) {
        switch side {
        case .right:
            print("Go right")
        case .left:
            print("Go leght")
        case .up:
            print("Go up")
        case .down:
            print("Go down")
        }
    }
}

let myCar = Car()
myCar.run()
myCar.turn(side: .up)



    
