//: A UIKit based Playground for presenting user interface
  
import UIKit
import PlaygroundSupport

class Engin {
    var isOkay : Bool = true
    var error  : String? = nil
}

class Car {
    
    enum Side {
        case right, left, up, down
    }
    
    private var isStarted : Bool = false
    private let engin = Engin()
    
    func start() -> Bool {
        if (isStarted) {
            return true
        }
        if !engin.isOkay {
            if let error = engin.error {
                print(error)
            }
            return false
        }
        isStarted = true
        return true
    }
    
    func run() -> Bool {
        if !isStarted {
            return false
        }
        return true
    }
    
    func runBack() -> Bool {
        if !isStarted {
            return false
        }
        
        return true
    }
    
    func turn(side: Side) {
        switch side {
        case .right:
            print("Go right")
        case .left:
            print("Go leght")
        case .up:
            print("Go up")
        case .down:
            print("Go down")
        }
    }
}

class SuperCar : Car {
    var helm : Helm
    
    init(helm : Helm) {
        self.helm = helm
    }
    
    override func turn(side: Side) {
        switch side {
        case .right:
            print("Go right")
        case .left:
            print("Go leght")
        case .up:
            print("Error")
        case .down:
            print("Error")
        }
    }
}

class Helm {
    let color : UIColor = .red
}

let myCar = Car()
myCar.run()
myCar.turn(side: .up)

let myHelm = Helm()

let mySuperCar = SuperCar(helm: myHelm)
mySuperCar.start()
mySuperCar.run()
mySuperCar.turn(side: .left)
