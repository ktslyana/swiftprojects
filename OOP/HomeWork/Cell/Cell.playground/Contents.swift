//: A UIKit based Playground for presenting user interface
  
import UIKit
import PlaygroundSupport

class View {
    var color : UIColor = .white
    var frame : CGRect
    
    init(frame : CGRect) {
        self.frame = frame
    }
    
    func addSubview(view: Any) {
        
    }
}

class NavigationBar {
    var color : UIColor = .white
    var frame : CGRect
    
    init(frame : CGRect) {
        self.frame = frame
    }
    
    func title(text: String) {
    }
    
    func setRightButton(icon: UIImage, action: Any) {
    }
    
    func setLeftButton(icon: UIImage, action: Any) {
    }
}

class ScrollView {
    var frame : CGRect
    private var objectsArray = [Cell]()
    
    init(frame : CGRect) {
        self.frame = frame
    }
    
    func add(cellArray: [Cell]) {
        objectsArray.append(contentsOf: cellArray)
    }
    
    func show() {
        var offset = 0
        for cellItem in objectsArray {
            cellItem.frame = CGRect(x: 0, y: offset, width: 320, height: 50)
            
            print(cellItem)
            
            offset = offset + 50
        }
    }
}

class Cell {
    var color : UIColor = .white
    var frame : CGRect
    
    private let icon : UIImage
    private let title : String
    private let date : Date
    
    init(frame : CGRect, icon : UIImage, title : String, date : Date) {
        self.frame = frame
        self.icon = icon
        self.title = title
        self.date = date
    }
}






let view = View(frame: CGRect(x: 0, y: 0, width: 320, height: 540))

let navigationBar = NavigationBar(frame: CGRect(x: 0, y: 0, width: 320, height: 40))
let scroll = ScrollView(frame: CGRect(x: 0, y: 40, width: 320, height: 500))

view.addSubview(view: navigationBar)
view.addSubview(view: scroll)

let cellArray = [
    Cell(frame: CGRect(x: 0, y: 0, width: 320, height: 50), icon: UIImage(), title: "Cell number 1", date: Date()),
    Cell(frame: CGRect(x: 0, y: 0, width: 320, height: 50), icon: UIImage(), title: "Cell number 2", date: Date()),
    Cell(frame: CGRect(x: 0, y: 0, width: 320, height: 50), icon: UIImage(), title: "Cell number 3", date: Date())
]
scroll.add(cellArray: cellArray)
scroll.show()
