import UIKit

class View {
    var collor : UIColor = .white
    var frame : CGRect
    
    init(frame:CGRect){
        self.frame = frame
    }
    func addSubView(view:Any){
        
    }
    
}

class NavigationBar{
    var collor : UIColor = .white
    var frame : CGRect
    
    
    init(frame:CGRect) {
        self.frame = frame
    }
    
    func leftButton(icon:UIImage,action:Any) {
        
    }
    func rightButton(icon:UIImage,acttion: Any){
        
    }
    func title(text:String){
        
    }
    
    func getMaxY() -> CGFloat {
        return self.frame.origin.y + self.frame.size.height
    }
}
class Text {
    var collor : UIColor = .black
    var frame : CGRect
    var text : String
    
    init(frame : CGRect,text : String){
        self.frame = frame
        self.text = text
    }
    
}
class Page {
    
    var collor : UIColor = .white
    var frame : CGRect
    
    private let description : String
    private let date : Date
    private let picture : UIImage
    private let name : String

    
    
    init(frame:CGRect,description : String,date : Date,picture : UIImage,name : String) {
        self.frame = frame
        self.description = description
        self.date = date
        self.picture = picture
        self.name = name
    }
    
    func show() {
        self.frame.size.height = self.calculateHeight()
        print("Page view")
    }
    
    private func calculateHeight() -> CGFloat {
        return 50 + 20 + picture.size.height + 50
    }
    
    func getMaxY() -> CGFloat {
        return self.frame.origin.y + self.frame.size.height
    }
}


class ScrollView {
    var frame : CGRect
    var text : Text? = nil
    
    init(frame:CGRect) {
        self.frame = frame
    }
    
    func add(text: Text) {
        self.text = text
    }
    func show() {
        
        print("Text view")
    }
    
    
       
   
}

let view = View(frame: CGRect(x: 0, y: 0, width: 320, height: 540))
let navigationBar = NavigationBar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
let page = Page(frame: CGRect(x: 0, y: navigationBar.getMaxY(), width: 320, height: 0), description: "Description", date: Date(), picture: UIImage(), name: "My Statue Name")
page.show()


let text = Text(frame: CGRect(), text: "some text")

let scroll = ScrollView(frame: CGRect(x: 0, y:page.getMaxY(), width: 320, height: 0))
scroll.add(text: text)
scroll.show()

