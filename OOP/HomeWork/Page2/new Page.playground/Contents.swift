//: Playground - noun: a place where people can play

import UIKit

class View {
    var collor : UIColor = .white
    var frame : CGRect
    
    init(frame:CGRect) {
        self.frame = frame
    }
    
    func addSubView(view:Any) {
        
    }
}

class NavigationBar {
    var collor : UIColor = .blue
    var frame : CGRect
    
    init(frame:CGRect) {
        self.frame = frame
    }
    
    func button(icon:UIImage,action:Any) {
        
    }
    func text(text:String){
        
    }
    func maxY() -> CGFloat {
        return self.frame.origin.y + self.frame.size.height
        
    }
}

class Page {
  private  var collor : UIColor = .white
   private var text : String = "Facebook video cover(851x315px)"
    var frame : CGRect
    
    init(frame:CGRect){
        self.frame = frame

    }
    func maxY() -> CGFloat {
        return self.frame.origin.y + self.frame.size.height
    }
}

class Search {
    var collor : UIColor = .gray
    var frame : CGRect
   private var placeholder : String = "Search for templates"
    private var text : String? = nil
    
    init(frame:CGRect,text:String) {
        self.frame = frame
        self.text = text
    }
    
    func returnText() -> String {
       return text!
    }
    func maxY() -> CGFloat {
        return self.frame.origin.y + self.frame.size.height
    }
    
}

class Pictures {
    var frame : CGRect
    var pictures : UIImage
    
    init(frame:CGRect,pictures:UIImage) {
        self.frame = frame
        self.pictures = pictures
    }
}

class ScrollView {
    var frame : CGRect
  private var picturesArray = [Pictures]()
    
    init(frame:CGRect) {
        self.frame = frame
    }
    func addPicture (pageArray : [Pictures]){
        picturesArray.append(contentsOf:pageArray)
    }
    func show(){
        var offset = 0
        for pictures in picturesArray {
            pictures.frame = CGRect(x: 0, y: 0, width: 320, height: 100)
            print(pictures)
            offset = offset + 50
        }
    }
}

let view = View(frame:CGRect(x: 0, y: 0, width: 320, height: 540))
let navigationBar = NavigationBar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
let page = Page(frame: CGRect(x: 0, y: navigationBar.maxY(), width: 320, height: 40))
let search = Search(frame: CGRect(x: 0, y: page.maxY(), width: 280, height: 30),text: "")
let scroll = ScrollView(frame: CGRect(x: 0, y: search.maxY(), width: 320, height: 0))
let pictures = Pictures(frame: CGRect(), pictures: UIImage())
scroll.addPicture(pageArray: [pictures])















