//: Playground - noun: a place where people can play

import UIKit

class Person {
    var name : String
    var lastname : String
    var surname : String
    var birthdayDay : Int
    var birthdayMonth : String
    var birthdayYear : Int
    
    init(name: String,lastname: String,surname: String,birthdayDay: Int,birthdayMonth: String,birthdayYear: Int) {
        self.name = name
        self.lastname = lastname
        self.surname = surname
        self.birthdayDay = birthdayDay
        self.birthdayMonth = birthdayMonth
        self.birthdayYear = birthdayYear
        
    }
    
    func howMuchYear() -> Int {
        let year = 2018 - birthdayYear
        return year
    }
}

class Candidate : Person {
    var number : Int
    var electorate : Int
    var votedForTheCandidate : Int
    var partyAffiliation : String
    init(name: String, lastname: String, surname: String, birthdayDay: Int, birthdayMonth: String, birthdayYear: Int, number: Int, electorate: Int,votedForTheCandidate : Int, partyAffiliation : String) {
        self.number = number
        self.electorate = electorate
        self.votedForTheCandidate = votedForTheCandidate
        self.partyAffiliation = partyAffiliation
        super.init(name: name,lastname: lastname,surname: surname,birthdayDay: birthdayDay,birthdayMonth: birthdayMonth,birthdayYear: birthdayYear)
        
    }
    
}
let nadia = Candidate.init(name: "Nadia", lastname: "Tverdohlib", surname: "Ihorivna", birthdayDay: 11, birthdayMonth: "October", birthdayYear: 1992, number: 136, electorate: 180, votedForTheCandidate: 5, partyAffiliation: "...")
let vasia = Candidate.init(name: "Vasia", lastname: "Maluk", surname: "Oleksndrovich", birthdayDay: 27, birthdayMonth: "February", birthdayYear: 1993, number: 136, electorate: 180, votedForTheCandidate: 95, partyAffiliation: "...")
let olexandr = Candidate.init(name: "Olexandr", lastname: "Maluk", surname: "Oleksndrovich", birthdayDay: 27, birthdayMonth: "February", birthdayYear: 1993, number: 136, electorate: 180, votedForTheCandidate: 80, partyAffiliation: "...")
let allCandidate = [nadia,vasia,olexandr]

for candiate in allCandidate {
    if candiate.votedForTheCandidate > 85 {
        
            
        }
        
        
}







