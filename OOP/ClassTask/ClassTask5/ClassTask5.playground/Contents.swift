//: Playground - noun: a place where people can play

import UIKit

class Person {
    var name : String
    var lastname : String
    var surname : String
    var birthdayDay : Int
    var birthdayMonth : String
    var birthdayYear : Int
    
    init(name: String,lastname: String,surname: String,birthdayDay: Int,birthdayMonth: String,birthdayYear: Int) {
        self.name = name
        self.lastname = lastname
        self.surname = surname
        self.birthdayDay = birthdayDay
        self.birthdayMonth = birthdayMonth
        self.birthdayYear = birthdayYear
        
    }
    
    func howMuchYear() -> Int {
        let year = 2018 - birthdayYear
        return year
    }
}
class BankCustomer : Person {
    var passportSeries : String
    var sex : String
    var loanAmount: Int
    var phone : String
    
    init(name: String, lastname: String, surname: String, birthdayDay: Int, birthdayMonth: String, birthdayYear: Int,passportSeries : String,sex : String, loanAmount: Int, phone : String) {
        self.passportSeries = passportSeries
        self.sex = sex
        self.loanAmount = loanAmount
        self.phone = phone
        super.init(name: name, lastname: lastname, surname: surname, birthdayDay : birthdayDay, birthdayMonth: birthdayMonth, birthdayYear: birthdayYear)
    }
}

let oleg1 = BankCustomer.init(name: "Oleg", lastname: "Hutsul", surname:"Bohdanovich", birthdayDay: 30, birthdayMonth: "March", birthdayYear: 1987, passportSeries: "24657342", sex: "boy", loanAmount: 20000, phone: "380664342468")
let oleg2 = BankCustomer.init(name: "Oleg", lastname: "Hutsul", surname:"Bohdanovich", birthdayDay: 30, birthdayMonth: "March", birthdayYear: 1987, passportSeries: "24657342", sex: "boy", loanAmount: 20000, phone: "380664342468")

let customer = [oleg1,oleg2]
var allBoyYear = 0
var allLoanAmout = 0

for people in customer {
    
    allLoanAmout += people.loanAmount
    
    if people.sex == "boy"{
        
        allBoyYear += people.howMuchYear()
    }
    
}

let middleBoyYear = allBoyYear / customer.count
print(middleBoyYear)
print(allLoanAmout)

