//: Playground - noun: a place where people can play

import UIKit

class Person {
    var name : String
    var lastname : String
    var surname : String
    var birthday : Int
    var birthdaymonth:String
    var birthdayyear:Int
    
    init(name:String, lastname : String,surname : String,birthday : Int,birthdaymonth : String,birthdayyear : Int) {
        self.name = name
        self.lastname = lastname
        self.surname = surname
        self.birthday = birthday
        self.birthdaymonth = birthdaymonth
        self.birthdayyear = birthdayyear
    }
    
    func howMuchYear() -> Int {
        let year = 2018 - self.birthdayyear
        return year
    }
}

class Applicant : Person {
    var faculty :String
    var specialty : String
    var middlePoint :Int
    init(name: String, lastname: String, surname: String, birthday: Int, birthdaymonth: String, birthdayyear: Int,faculty: String,specialty: String,middlePoint: Int){
        self.faculty = faculty
        self.specialty = specialty
        self.middlePoint = middlePoint
    super.init(name: name, lastname: lastname, surname: surname, birthday: birthday, birthdaymonth: birthdaymonth, birthdayyear: birthdayyear)
}
}
let anastasia = Applicant.init(name: "Anastasia", lastname: "", surname: "", birthday: 20, birthdaymonth: "March", birthdayyear: 1997, faculty: "Fmi", specialty: "teacher", middlePoint: 4)
let petro = Applicant.init(name: "Porto", lastname: "", surname: "", birthday: 8, birthdaymonth: "July", birthdayyear: 1998, faculty: "Fmi", specialty: "teacher", middlePoint: 3)
let vasul = Applicant.init(name: "Vasul", lastname: "", surname: "", birthday: 30, birthdaymonth: "October", birthdayyear: 1998, faculty: "Fiz", specialty: "fiz", middlePoint: 5)
let aplicants = [anastasia,petro,vasul]
var FmiAplicant = [String]()

for person in aplicants {
    if person.faculty == "Fmi" {
        FmiAplicant.append(person.name)
    }
}



















