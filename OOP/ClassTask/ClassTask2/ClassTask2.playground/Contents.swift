//: Playground - noun: a place where people can play

import UIKit

class Persons {
    var name : String
    var lastname : String
    var surname : String
    var birthdayDay : Int
    var birthdayMonth : String
    var birthdayYear : Int
    
    init(name: String,lastname: String,surname: String,birthdayDay: Int,birthdayMonth: String,birthdayYear: Int) {
        self.name = name
        self.lastname = lastname
        self.surname = surname
        self.birthdayDay = birthdayDay
        self.birthdayMonth = birthdayMonth
        self.birthdayYear = birthdayYear
        
    }
    
    func howMuchYear() -> Int {
        let year = 2018 - birthdayYear
        return year
    }
}

class Driver : Persons {
    var distance : Int
    var priceOneKilometer : Int
    var carBrand : String
    
     init(name: String, lastname: String, surname: String, birthdayDay: Int, birthdayMonth: String, birthdayYear: Int,distance: Int,priceOneKilometr: Int,carBrand: String) {
        self.distance = distance
        self.priceOneKilometer = priceOneKilometr
        self.carBrand = carBrand
        super.init(name: name, lastname: lastname, surname: surname, birthdayDay: birthdayDay, birthdayMonth: birthdayMonth, birthdayYear: birthdayYear)
    }
}
let yana = Driver.init(name: "Yana", lastname:"Kitsul", surname: "Yaroslavivna", birthdayDay: 11,birthdayMonth: "August", birthdayYear: 1998,distance: 10,priceOneKilometr: 28,carBrand: "BMW")
let roma = Driver.init(name: "Roma", lastname: "Koreiba", surname: "Vasiliovich", birthdayDay: 23, birthdayMonth: "March", birthdayYear: 1973, distance: 250, priceOneKilometr: 20, carBrand: "Lada")

let yanaPrice = yana.distance * yana.priceOneKilometer
print(yanaPrice)
let romaPrice = roma.distance * roma.priceOneKilometer
print(romaPrice)

let allDriver = [yana,roma]
















