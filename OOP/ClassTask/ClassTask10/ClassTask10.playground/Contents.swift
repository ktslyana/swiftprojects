//: Playground - noun: a place where people can play

import UIKit

class Person {
    var name : String
    var lastname : String
    var surname : String
    var birthday : Int
    var birthdaymonth:String
    var birthdayyear:Int
    
    init(name:String, lastname : String,surname : String,birthday : Int,birthdaymonth : String,birthdayyear : Int) {
        self.name = name
        self.lastname = lastname
        self.surname = surname
        self.birthday = birthday
        self.birthdaymonth = birthdaymonth
        self.birthdayyear = birthdayyear
    }
    
    func howMuchYear() -> Int {
        let year = 2018 - self.birthdayyear
        return year
    }
}
class Doctor : Person {
    var specialty : String
    var sizeWages : Int
    var patients : Int
    
    init(name: String, lastname: String, surname: String, birthday: Int, birthdaymonth: String, birthdayyear: Int,specialty: String,sizeWages: Int,patients: Int) {
        self.specialty = specialty
        self.sizeWages = sizeWages
        self.patients = patients
        super.init(name: name, lastname: lastname, surname: surname, birthday: birthday, birthdaymonth: birthdaymonth, birthdayyear: birthdayyear)
    }
}

let vasul = Doctor.init(name: "Vasul", lastname: "", surname: "Vasulovich", birthday: 20, birthdaymonth: "October", birthdayyear: 1953, specialty: "doctor", sizeWages: 6000, patients: 1000)
let olga = Doctor.init(name: "Olga", lastname: "", surname: "Petrovna", birthday: 8, birthdaymonth: "March", birthdayyear: 1973, specialty: "doctor", sizeWages: 4500, patients: 732)
let doctors = [vasul,olga]
var allPaients = 0
var middleWages = 0
for person in doctors {
   allPaients += person.patients
    middleWages += person.sizeWages / doctors.count
    }
print(allPaients)
print(middleWages)
    
    
    
    
    
    
    
    
    
    
