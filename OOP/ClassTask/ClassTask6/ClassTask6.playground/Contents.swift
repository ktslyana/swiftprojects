//: Playground - noun: a place where people can play

import UIKit

class Person {
    var name : String
    var lastname : String
    var surname : String
    var birthdayDay : Int
    var birthdayMonth:String
    var birthdayYear:Int
    
    init(name:String, lastname : String,surname : String,birthdayDay : Int,birthdayMonth : String,birthdayYear : Int) {
        self.name = name
        self.lastname = lastname
        self.surname = surname
        self.birthdayDay = birthdayDay
        self.birthdayMonth = birthdayMonth
        self.birthdayYear = birthdayYear
    }
    
    func howMuchYear() -> Int {
        let year = 2018 - self.birthdayYear
        return year
    }
}
class Retiree : Person {
    var yearOfRetirement : Int
    var sex : String
    var theSizeOfThePension : Int
    var experience : Int
    init(name: String, lastname: String, surname: String, birthdayDay: Int, birthdayMonth: String, birthdayYear: Int,yearOfRetirement : Int, sex : String, theSizeOfThePension : Int, experience : Int) {
        self.yearOfRetirement = yearOfRetirement
        self.sex = sex
        self.theSizeOfThePension = theSizeOfThePension
        self.experience = experience
        super.init(name: name, lastname: lastname, surname: surname, birthdayDay: birthdayDay, birthdayMonth: birthdayMonth, birthdayYear: birthdayYear)
        
    }
    func allPension() -> Int {
        let allPension = ((2018 - self.yearOfRetirement) * self.theSizeOfThePension)
        return allPension
    }
}

let marta = Retiree.init(name: "Marta", lastname: "Folk", surname: "Ihorivna", birthdayDay: 24, birthdayMonth: "October", birthdayYear: 1944, yearOfRetirement: 2000, sex: "girl", theSizeOfThePension: 2000, experience: 46)
let oleg = Retiree.init(name: "Oleg", lastname: "Hutsul", surname:"Bohdanovich", birthdayDay: 30, birthdayMonth: "March", birthdayYear: 1957, yearOfRetirement: 2001 , sex: "boy", theSizeOfThePension: 1500, experience: 40)
let olena = Retiree.init(name: "Olena", lastname: "Vasuluk", surname: "Yarovilovivna", birthdayDay: 11, birthdayMonth: "March", birthdayYear: 1930, yearOfRetirement: 2002, sex: "girl", theSizeOfThePension: 1700, experience: 42)
let allRetiree = [marta,oleg,olena]
var allYearOfRetirement = 0
var allPension = 0


for retiree in allRetiree {
    if retiree.sex == "girl" {

        allYearOfRetirement += retiree.howMuchYear() - (2018 - retiree.yearOfRetirement)
        
        
    }
}

let middleYearOfRetirement = allYearOfRetirement / allRetiree.count
print(middleYearOfRetirement)

print(marta.allPension())
print(oleg.allPension())
print(olena.allPension())

