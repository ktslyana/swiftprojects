//: Playground - noun: a place where people can play

import UIKit

class Person {
    var name : String
    var lastname : String
    var surname : String
    var birthday : Int
    var birthdaymonth:String
    var birthdayyear:Int
    
    init(name:String, lastname : String,surname : String,birthday : Int,birthdaymonth : String,birthdayyear : Int) {
        self.name = name
        self.lastname = lastname
        self.surname = surname
        self.birthday = birthday
        self.birthdaymonth = birthdaymonth
        self.birthdayyear = birthdayyear
    }
    
    func howMuchYear() -> Int {
        let year = 2018 - self.birthdayyear
        return year
    }
}

class Teacher : Person {
    var faculty : String
    var position : String
    var academicStatus : String
    var salary : Int
    var howMuchCourses : Int
    init(name: String, lastname: String, surname: String, birthday: Int, birthdaymonth: String, birthdayyear: Int,faculty: String,position: String,academicStatus: String,salary: Int,howMuchCourses : Int) {
        self.faculty = faculty
        self.position = position
        self.academicStatus = academicStatus
        self.salary = salary
        self.howMuchCourses = howMuchCourses
        super.init(name: name, lastname: lastname, surname: surname, birthday: birthday, birthdaymonth: birthdaymonth, birthdayyear: birthdayyear)
    }
    
}
let vasul = Teacher.init(name: "Vasul", lastname: "Hruhorovich", surname: "Yaroslavovich", birthday: 17, birthdaymonth: "September", birthdayyear: 1968, faculty: "Math", position: "dean", academicStatus: "dean", salary: 5000, howMuchCourses: 2)
let ihor = Teacher.init(name: "Ihor", lastname: "Vasuluk", surname: "Ihorovich", birthday: 8, birthdaymonth: "March", birthdayyear: 1976, faculty: "Math", position: "lecturer", academicStatus: "lecturer", salary: 3500, howMuchCourses: 3)
var  allYear = 0
var allCourses = 0
let allTeacher = [vasul,ihor]

for teacher in allTeacher {
    allCourses += teacher.howMuchCourses
    allYear += teacher.howMuchYear()
    }
let middleYear = allYear / allTeacher.count
print(allCourses)
print(middleYear)
