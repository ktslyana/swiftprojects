//: Playground - noun: a place where people can play

import UIKit

class Person {
    var name : String
    var lastname : String
    var surname : String
    var birthday : Int
    var birthdaymonth:String
    var birthdayyear:Int
    
    init(name:String, lastname : String,surname : String,birthday : Int,birthdaymonth : String,birthdayyear : Int) {
        self.name = name
        self.lastname = lastname
        self.surname = surname
        self.birthday = birthday
        self.birthdaymonth = birthdaymonth
        self.birthdayyear = birthdayyear
    }
    
    func howMuchYear() -> Int {
        let year = 2018 - self.birthdayyear
        return year
    }
}

class Student : Person {
    var faculty : String
    var studentship : Int
    var gpa : Int
    var grup : Int
    init(name:String, lastname : String,surname : String,birthday : Int,birthdaymonth : String,birthdayyear : Int,faculty : String,grup : Int,studentship : Int , gpa : Int) {
        self.faculty = faculty
        self.grup = grup
        self.studentship = studentship
        self.gpa = gpa
        super.init(name: name, lastname: lastname, surname: surname, birthday: birthday, birthdaymonth: birthdaymonth,birthdayyear: birthdayyear)
    }
    
}
let yana = Student.init(name: "Yana", lastname:"Kitsul", surname: "Yaroslavivna", birthday: 11,birthdaymonth: "August", birthdayyear: 1998,faculty: "Math",grup: 402,studentship: 500,gpa: 4)
let sofia = Student.init(name: "Sofia", lastname:"Evdokimova", surname: "Ihorivna", birthday: 27,birthdaymonth: "August", birthdayyear: 1999,faculty: "Math",grup: 402,studentship: 600,gpa: 5)
let roman = Student.init(name: "Roman", lastname:"Koreiba", surname: "Yaroslavovich", birthday: 25,birthdaymonth: "August", birthdayyear: 1997,faculty: "Math",grup: 402,studentship: 0,gpa: 3)

let student = [yana,sofia,roman]
var allStudentship = 0
var allYear = 0
for people in student{
    allStudentship += people.studentship
    allYear += people.howMuchYear()
    
}
let middleYear = allYear/student.count
print(middleYear)
print(allStudentship)









