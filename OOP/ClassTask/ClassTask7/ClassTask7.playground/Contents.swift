//: Playground - noun: a place where people can play

import UIKit

class Person {
    var name : String
    var lastname : String
    var surname : String
    var birthdayDay : Int
    var birthdayMonth : String
    var birthdayYear : Int
    
    init(name: String,lastname: String,surname: String,birthdayDay: Int,birthdayMonth: String,birthdayYear: Int) {
        self.name = name
        self.lastname = lastname
        self.surname = surname
        self.birthdayDay = birthdayDay
        self.birthdayMonth = birthdayMonth
        self.birthdayYear = birthdayYear
        
    }
    
    func howMuchYear() -> Int {
        let year = 2018 - birthdayYear
        return year
    }
}
class Patient: Person {
    var sex : String
    var weight : Int
    var gain : Int
    var diagnosis : String
    init(name: String, lastname: String, surname: String, birthdayDay: Int, birthdayMonth: String, birthdayYear: Int,sex: String,weight: Int,gain: Int,diagnosis: String) {
        self.sex = sex
        self.weight = weight
        self.gain = gain
        self.diagnosis = diagnosis
        super.init(name: name, lastname: lastname, surname: surname, birthdayDay: birthdayDay, birthdayMonth: birthdayMonth, birthdayYear: birthdayYear)
    }
}

let roma = Patient.init(name: "Roma", lastname: "", surname: "", birthdayDay: 9, birthdayMonth: "October", birthdayYear: 1996, sex: "boy", weight: 56, gain: 165, diagnosis: "")
let nastia = Patient.init(name: "Nasia", lastname: "", surname: "", birthdayDay: 28, birthdayMonth: "March", birthdayYear: 1995, sex: "girl", weight: 65, gain: 173, diagnosis: "")
let viktor = Patient.init(name: "Viktor", lastname: "", surname: "", birthdayDay: 6, birthdayMonth: "February", birthdayYear: 1995, sex: "boy", weight: 87, gain: 194, diagnosis: "")

let patients = [roma,nastia,viktor]
var middlegain = 0
for person in patients {
    if person.sex == "boy" {
        middlegain += person.gain / patients.count
    }
}










