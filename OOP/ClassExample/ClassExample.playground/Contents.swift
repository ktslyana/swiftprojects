//: Playground - noun: a place where people can play

import UIKit

class User{
    
    var name: String
    var surname: String
    
    init(name: String, surname: String){
        
        self.name = name
        self.surname = surname
    }
    
    func getFullInfo() -> String{
        
        return "\(self.name) \(self.surname)"
    }
}

class Employee : User{
    
    var company: String
    init(name: String, surname: String, company: String){
        
        self.company = company
        super.init(name: name, surname: surname)
    }
}

let emp: Employee = Employee(name: "Steve", surname: "Jobs", company:"Apple")
let emplInfo = emp.getFullInfo()
print(emplInfo)




