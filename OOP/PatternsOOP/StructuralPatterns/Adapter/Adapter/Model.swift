//
//  Model.swift
//  Adapter
//
//  Created by Vitalii Todorovych on 7/18/19.
//  Copyright © 2019 Correnet. All rights reserved.
//

import Foundation
import UIKit

protocol AddTextToUIViewControllerProtocol {
    func AddText(text:String)
}

class Adapter: UIViewController,AddTextToUIViewControllerProtocol {
    func AddText(text: String) {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 100))
        label.text = text
        label.textColor = UIColor.black
        
        self.view.addSubview(label)
        label.center = view.center
    }
    
    
}
