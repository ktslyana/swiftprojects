//: Playground - noun: a place where people can play

import UIKit

protocol VideoServiceProtocol {
    func loadVideoList(url:URL?) -> [Data]?
}

class VideoSevice : VideoServiceProtocol {
    func loadVideoList(url: URL?) -> [Data]?{
        // завантаження з інтернету
        return []
    }
}
    
    class Proxy:VideoServiceProtocol {
        var service:VideoServiceProtocol
        init(service:VideoServiceProtocol){
            self.service = service
        }
        private var cashData: [Data]?
        func loadVideoList(url: URL?) -> [Data]?{
            if cashData == nil {
                cashData = service.loadVideoList(url: url)
            }
         
            return cashData
        }
        }
    
    
var videoService = VideoSevice()
var proxy = Proxy(service: VideoSevice)
 
