//
//  ViewController.swift
//  Bridge
//
//  Created by Vitalii Todorovych on 7/18/19.
//  Copyright © 2019 Correnet. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDataSource {
 

    @IBOutlet var TableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
       
       TableView.dataSource = self
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
}

