//: Playground - noun: a place where people can play

import UIKit


class ExerciseFactory { //фабрика
    var exercises: [Exercise] = []
    
    func createExercice(id:Int) -> Exercise {
        
        for exercise in exercises {
            if exercise.id == id {
                return exercise
            }
        }
        let e = Exercise()
        exercises.append(e)
        return e
    }
}



class Exercise {
    var id : Int?
    var name: String?
    var escription: String?
    
    var image: UIImage?
    var step: [UIImage]?
    var video: Any?
}



class ExerciseInWorckout {

    var exerxice: Exercise?
    var time: Double?
}

class Worckout {
    var exercise: [Exercise]?
    
    func start() {
        
    }
    func stop() {
        
    }
}
