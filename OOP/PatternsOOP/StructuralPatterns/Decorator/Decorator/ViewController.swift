//
//  ViewController.swift
//  Decorator
//
//  Created by Vitalii Todorovych on 7/18/19.
//  Copyright © 2019 Correnet. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        var element: Element = SimpleView(view:UIView(frame: CGRect(x: 0, y: 0, width: 300, height: 300)))
        element = UpgradeBorer(element:element)
        view.addSubview(element.view)
        print(element.description)
    }


}

