//
//  Decorator.swift
//  Decorator
//
//  Created by Vitalii Todorovych on 7/18/19.
//  Copyright © 2019 Correnet. All rights reserved.
//

import Foundation
import UIKit

protocol Element {
    var view : UIView{get}
    var description : String{get}
}

class SimpleView : Element {
    var view: UIView
    var description: String
    
    init(view:UIView) {
        self.view = view
        self.description = "Simple View"
    }
}

class Decorator: Element {
    var view: UIView
    var description: String
    init(element:Element) {
        self.view = element.view
        self.description = element.description
    }
    
}

class UpgradeBorer: Decorator {
    override init(element:Element) {
        super.init(element: element)
        self.view.layer.borderColor = UIColor.black.cgColor
        self.description = element.description + "+ Border Collor"
    }
}
