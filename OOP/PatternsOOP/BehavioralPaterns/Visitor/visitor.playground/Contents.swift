//: Playground - noun: a place where people can play

import UIKit



protocol Visitor {
    func visit(person:Person)
    func visit(note:Note)
}
protocol Acceptor {
    func showData(visitir:Visitor)
    
}

class Person: Acceptor{
    var name: String = ""
    var surname: String = ""
    var email:String = ""
    
    func showData(visitir:Visitor){
       visitir.visit(person: self)
    }
}

class Note: Acceptor {
    var title: String = ""
    var text: String = ""
    
    func showData(visitir:Visitor) {
       visitir.visit(note: self)
    }
}

class GeneratorTXT: Visitor {
    var txt: String = ""
    func visit(person:Person){
        txt += person.name + "," + person.surname+ "," + person.email
    }
    func visit(note:Note){
        txt += note.title + "," + note.text
 }
}
