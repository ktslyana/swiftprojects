//: Playground - noun: a place where people can play

import UIKit



protocol Chain {
    var nextStep: Chain? {get set}
    func check() -> (isSeccess: Bool, error: String)
}
class NetworkManager: Chain {
    var nextStep: Chain?
    func check() -> (isSeccess: Bool, error: String) {
        if checkNetwork() {
            if checkConnectionSpeed() > 3 {
                if nextStep != nil {
                    return (nextStep!.check().isSeccess,nextStep!.check().error)
                }
                else {
                    return(true,"")
                }
                
            }
            else {
                return(false,"Small Speed")
            }
        }
            else {
                return(false,"net Unterneta")
            }
        
    }
    func checkNetwork() -> Bool {
        return true
    }
    func checkConnectionSpeed() -> Double {
        return 10.0
    }
}

class DounladManager: Chain {
    var nextStep: Chain?
    func check() -> (isSeccess: Bool, error: String) {
        if isServerAvilable(){
            if nextStep != nil {
                return (nextStep!.check().isSeccess,nextStep!.check().error)
            }
            else {
                return(true,"")
            }
        }
            else {
                return(false,"Server ne dostypen")
            }
        
    }
    func isServerAvilable() -> Bool {
        return true
    }
}

class DeviceManager: Chain {
    var nextStep: Chain?
    func check() -> (isSeccess: Bool, error: String) {
        
        if nextStep != nil {
            return (nextStep!.check().isSeccess,nextStep!.check().error)
        }
        else {
            return (true,"")
        }
    }
    
    func iOSVersion() -> String {
        return "10.3"
    }
}

class UpdateManager {
    var networkManager = NetworkManager()
    var dounladManager = DounladManager()
    var deviceManager = DeviceManager()
    
    func update(){
        networkManager.nextStep = dounladManager
        dounladManager.nextStep = deviceManager
        
        if networkManager.check().isSeccess {
            //obnov
        } else {
            print("oshubka:\(networkManager.check().error)")
        }
    }
}
var u = UpdateManager()
u.update
