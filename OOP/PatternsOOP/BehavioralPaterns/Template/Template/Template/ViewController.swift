//
//  ViewController.swift
//  Template
//
//  Created by Vitalii Todorovych on 7/19/19.
//  Copyright © 2019 Correnet. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var button1: AllertButton!
    @IBOutlet var button2: WarningButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        button1.initialize()
        button2.initialize()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

