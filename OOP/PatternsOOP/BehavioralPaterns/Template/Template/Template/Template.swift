//
//  Template.swift
//  Template
//
//  Created by Vitalii Todorovych on 7/19/19.
//  Copyright © 2019 Correnet. All rights reserved.
//

import Foundation
import UIKit

class ButtonTemplate: UIButton {
    func setCollorTitle() {
        
    }
    func setCollorBackground() {
        
    }
    func setTitle() {
        
    }
    
    func initialize() {
        setCollorTitle()
        setCollorBackground()
        setTitle()
    }
}

class AllertButton: ButtonTemplate {
    override func setTitle() {
        self.setTitle("Alert",for: .normal)
    }
}

class WarningButton:ButtonTemplate {
    override func setCollorTitle() {
        self.setTitleColor(UIColor.red, for: .normal)
    }
    override func setTitle() {
        self.setTitle("Warning",for: .normal)
    }
}
