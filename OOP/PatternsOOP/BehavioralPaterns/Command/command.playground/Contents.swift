//: Playground - noun: a place where people can play

import UIKit



protocol Command {
    func on()
    func off()
}
class Heating {
    func setDegree(value:Int) {
        print("vkl na \(value) hradysiv")
    }
    func off() {
        print("vukl teplo")
    }
}
class HeatingCommand : Command {
    
    var heating:Heating
    init(heating:Heating){
        self.heating = heating
    }
    func on(){
        self.heating.setDegree(value: 39)
    }
    func off() {
        self.heating.off()
    }
    
}

class Light {
    func on() {
        print("vkl svet")
    }
    func off()  {
        print("vukl svet")
    }
}

class LightCommand: Command {
    var light:Light
    init(lignt:Light){
        self.light = lignt
    }
    func on() {
        self.light.on()
    }
    
    func off() {
        self.light.off()
    }
    
    
}

class Sound {
    func playMusik(sound:String){
        print("play sound \(sound)")
    }
    func stop() {
        print("stop sound")
    }
}

class SoundCommand: Command {
    var sound:Sound
    init(sound:Sound) {
        self.sound = sound
    }
    
    func on() {
        self.sound.playMusik(sound: "lady gaga")
    }
    
    func off() {
        self.sound.stop()
    }
    
    
}


var light = Light()
var sound = Sound()
var heating = Heating()


func on() {
    LightCommand(lignt: light).on()
    SoundCommand(sound: sound).on()
    HeatingCommand(heating: heating).on()
}













