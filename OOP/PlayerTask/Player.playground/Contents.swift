//: Playground - noun: a place where people can play

import UIKit

class Equalizer {
    var volume : Int = 0
    var bass : Int = 0
    
    func volumeLevel(volume:Int)  {
        print("Volume level \(volume)")
    }
    
    func basLevel(bass:Int) {
        print("Bass level \(bass)")
    }
}

class Player {
    private var onPlayer : Bool = false {
        didSet {
            if onPlayer {
                print("Player on")
            } else {
                print("Player Off")
            }
        }
    }
    private var playButton = UIButton()
    private var stopMusikButton = UIButton()
    var equalizer : Equalizer {
        didSet {
            applayEqualizer()
        }
    }
    
    init(equalizer : Equalizer) {
        self.equalizer = equalizer
    }
    
    @objc private func setupButtons() {
        playButton.addTarget(self, action: #selector(setupButtons), for: .touchUpInside)
        stopMusikButton.addTarget(self, action: #selector(stopActions), for: .touchUpInside)
    }
    
    @objc private func playActions() {
        setPlayer(on: true)
    }
    
    @objc private func stopActions() {
        setPlayer(on: false)
    }
    
    private func applayEqualizer() {
        print("Sound value:\(equalizer.volume)")
        print("Bass value:\(equalizer.bass)")
    }
    
    
    
    
    // PUBLIC
    func setPlayer(on:Bool) {
        onPlayer = on
    }
}

class CDPlayer : Player {
    var onPlayerCD : Bool = false {
        didSet {
            if onPlayerCD {
                print("CD Player on")
            } else {
                print("CD Player Off")
            }
        }
    }
    
    private var hasCD: Bool = false
    
    // PUBLIC
    override func setPlayer(on:Bool) {
        if hasCD {
            onPlayerCD = on
        }
    }
    
    func insertCD() {
        hasCD = true
    }
    
    func deleteCD() {
        hasCD = false
    }
}

class CassettePlayer : Player {
    var onCassettePlayer : Bool = false {
        didSet {
            if onCassettePlayer {
                print("Cassette player on")
            } else {
                print("Cassette player off")
            }
        }
    }
    
    private var hasCassette : Bool = false
    
    override func setPlayer(on: Bool) {
        if hasCassette {
            onCassettePlayer = on
        }
    }
    
    func insertCassette() {
        hasCassette = true
    }
    
    func deleteCassette() {
        hasCassette = false
    }
}





let equalizer = Equalizer()
let player = Player(equalizer: equalizer)
let cdPlayer = CDPlayer(equalizer: equalizer)
let cassettePlayer = CassettePlayer(equalizer: equalizer)
equalizer.bass = 70





