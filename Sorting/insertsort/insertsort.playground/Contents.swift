//: Playground - noun: a place where people can play

let sortedMasiv = insertsort(zrostania: false,masiv:[3,6,8,2])
print(sortedMasiv)

func insertsort(zrostania:Bool,masiv:[Int]) -> [Int]{
    
    var result = masiv
    for i in 1..<masiv.count{
        
        let elementI = result[i]
        var j = i-1
       
        if zrostania {
            while j >= 0 && result[j] > elementI {
                result[j+1] = result[j]
                j = j-1
                
            }
        } else {
            while j >= 0 && result[j] < elementI {
                result[j+1] = result[j]
                j = j-1
                
            }
        }
        
        result[j+1] = elementI
       
    }
    return result
}
