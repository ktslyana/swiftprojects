//: Playground - noun: a place where people can play

import UIKit

import UIKit

enum Smile :String {
    case joy = " :) "
    case laught = " :D "
    case sorrow = " :( "
    case surprise = " O_o "
    var emotion : String{return self.rawValue}
    func description() {
        print("contains a list of emoticons: their name and graphic description")    }
}
var mySmile = Smile.joy
mySmile.description() // methods
mySmile.emotion //properties
