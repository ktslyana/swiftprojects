//: Playground - noun: a place where people can play

import UIKit


enum CurrencyUnit {
    enum DollarCountrys {
        case USA
        case Canada
        case Australia
    }
    case Hryvnia(countrys: [String],shortname: String)
    case Rouble(countrys: [String],shortname: String)
    case Euro(countrys: [String],shortname: String)
    case Dollar(countrys: [String],shortname: String,national: DollarCountrys)
    
}
var dollarCurrency =  CurrencyUnit.Dollar(countrys: ["USA"], shortname: "USD", national: .USA)
print(dollarCurrency)