//: Playground - noun: a place where people can play

import UIKit

struct User {
    var name: String = "Tom"
    var age: Int = 18
    
    func getInfo() -> String{
        
        return "Имя: \( name). Возраст: \(age)"
    }
}
var tom: User = User()
print(tom.getInfo())    

var bob  = User()
bob.name = "Bob"
bob.age = 23
print(bob.getInfo())
