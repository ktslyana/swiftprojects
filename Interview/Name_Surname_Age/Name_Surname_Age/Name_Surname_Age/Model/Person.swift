//
//  Person.swift
//  Name_Surname_Age
//
//  Created by Vitalii Todorovych on 12/16/19.
//  Copyright © 2019 Correnet. All rights reserved.
//

struct Person {
    let name: String
    let surName: String
    let edge: Int
}
