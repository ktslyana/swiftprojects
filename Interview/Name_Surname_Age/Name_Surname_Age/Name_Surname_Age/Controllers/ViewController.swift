//
//  ViewController.swift
//  Name_Surname_Age
//
//  Created by Vitalii Todorovych on 12/16/19.
//  Copyright © 2019 Correnet. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var profileInputView: ProfileInputView!
    @IBOutlet weak var profileView: ProfileView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        profileInputView.delegate = profileView
        
        profileInputView.handleClosure = { inputedPerson in
            self.profileView.configure(person: inputedPerson)
        }
    }
    
}

