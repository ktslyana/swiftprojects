//
//  ProfileView.swift
//  Name_Surname_Age
//
//  Created by Vitalii Todorovych on 12/16/19.
//  Copyright © 2019 Correnet. All rights reserved.
//

import UIKit

class ProfileView: UIView, HandlePersonDelegate {
    
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var surnameLabel: UILabel!
    @IBOutlet private weak var ageLabel: UILabel!
    
    func configure(person: Person) {
        nameLabel.text = person.name
        surnameLabel.text = person.surName
        ageLabel.text = String(person.edge)
    }
    
    func handle(profileOfPerson: Person) {
        self.configure(person: profileOfPerson)
    }
}
