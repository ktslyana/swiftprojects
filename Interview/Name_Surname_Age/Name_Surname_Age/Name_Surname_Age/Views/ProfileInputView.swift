//
//  ProfileInputView.swift
//  Name_Surname_Age
//
//  Created by Vitalii Todorovych on 12/16/19.
//  Copyright © 2019 Correnet. All rights reserved.
//

import UIKit

protocol HandlePersonDelegate {
    func handle(profileOfPerson: Person)
}

class ProfileInputView: UIView {
    
    @IBOutlet private weak var nameTextField: UITextField!
    @IBOutlet private weak var surnameTextField: UITextField!
    @IBOutlet private weak var ageTextField: UITextField!
    
    //Use delegate for return inputed values
//    var delegate: HandlePersonDelegate?
    
    //Or by closures
    var handleClosure: ((_ inputedPerson: Person) -> Void)? = nil
    
    @IBAction func saveButtonAction(_ sender: UIButton) {
        
        guard let name = nameTextField.text,
            let surName = surnameTextField.text,
            let ageText = ageTextField.text,
            let ageInt = Int(ageText) else {
            return
        }
        
        let person = Person(name: name, surName: surName, edge: ageInt)
        
        handleClosure?(person)
//        delegate?.handle(profileOfPerson: person)
    }
}
