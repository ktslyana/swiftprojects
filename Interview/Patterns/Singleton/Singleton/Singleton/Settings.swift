//
//  File.swift
//  Singleton
//
//  Created by Vitalii Todorovych on 1/25/20.
//  Copyright © 2020 Correnet. All rights reserved.
//

import UIKit

class Settings {
  static let settings = Settings()
    
    var collor = UIColor.white
    var volumeLevel : Float = 1.0
    
    private init () {}
}
