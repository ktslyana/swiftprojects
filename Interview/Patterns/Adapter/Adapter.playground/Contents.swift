//: Playground - noun: a place where people can play

import UIKit

protocol BirdProtocol {
    func sing()
    func fly()
}

class Bird: BirdProtocol {
    func sing() {
        print("tew - tew")
    }
    
    func fly() {
        print("Omg! I am flying!")
    }
}

class Voron {
    func voronSing() {
        print("Kar - Kar")
    }
    
    func voronFly() {
        print("I am Fly!")
    }
}

class VoronAdapter : BirdProtocol {
    private var voron = Voron()
    
    init(adaptee:Voron) {
        voron = adaptee
    }
    func sing() {
        voron.voronSing()
    }
    
    func fly() {
        voron.voronFly()
    }
}

func makeTheBirdTest(bird:BirdProtocol) {
    bird.fly()
    bird.sing()
}

let simpleBird = Bird()
let simpleVoron = Voron()
let voronAdapter = VoronAdapter(adaptee: simpleVoron)

makeTheBirdTest(bird: simpleBird)
makeTheBirdTest(bird: voronAdapter)
