//
//  ViewController.swift
//  UIStackView
//
//  Created by Vitalii Todorovych on 12/12/19.
//  Copyright © 2019 Correnet. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var verticalStackView: UIStackView!
    
    @IBOutlet weak var horisontalStackView: UIStackView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    let collorsArray = [
        UIColor(red: 20/255.0, green: 187/255.0, blue: 181/255.0, alpha: 1.0),
        UIColor(red: 222/255.0, green: 56/255.0, blue: 71/255.0, alpha: 1.0),
        UIColor(red: 35/255.0, green: 174/255.0, blue: 183/255.0, alpha: 1.0),
        UIColor(red: 20/255.0, green: 153/255.0, blue: 45/255.0, alpha: 1.0)
    ]
    
    let url = ["https://www.google.com.ua/search?q=квіти&client=safari&hl=ru-ua&prmd=imvn&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjitZD5rrrmAhVaAxAIHe9iAE0Q_AUoAXoECA4QAQ&biw=375&bih=553&dpr=2#imgrc=OAxIBVEzrzCPCM","https://www.google.com.ua/search?q=квіти&client=safari&hl=ru-ua&prmd=imvn&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjitZD5rrrmAhVaAxAIHe9iAE0Q_AUoAXoECA4QAQ&biw=375&bih=553&dpr=2#imgrc=aFeUSNOMJcMLUM","https://www.google.com.ua/search?q=квіти&client=safari&hl=ru-ua&prmd=imvn&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjitZD5rrrmAhVaAxAIHe9iAE0Q_AUoAXoECA4QAQ&biw=375&bih=553&dpr=2#imgrc=hpNhHGPSWg3fUM"]
    
    func randomCollor() -> UIColor {
        let arrayCount = Int32(url.count)
        let ranomNumber = arc4random_uniform(UInt32(arrayCount))
        let number = Int(ranomNumber)
        
        return url[number]
    }
    func randomURL() -> String {
        let arrayCount = Int32(collorsArray.count)
        let ranomNumber = arc4random_uniform(UInt32(arrayCount))
        let number = Int(ranomNumber)
        
        return collorsArray[number]
    }

    @IBAction func addNewButtonAction(_ sender: UIButton) {
        
        let button = UIButton(type: UIButtonType.system)
        button.setTitle("New", for: UIControlState.normal)
        button.setTitleColor(UIColor.white, for: UIControlState.normal)
        button.backgroundColor = randomCollor()
        button.titleLabel?.font = UIFont.systemFont(ofSize: 40.0, weight: UIFont.Weight.light)
        button.isHidden = true
        button.addTarget(self, action: Selector(("DeleteButtonAction:")), for: UIControlEvents.touchUpInside)
        
        self.verticalStackView.insertArrangedSubview(button, at: 1)
        
        UIView.animate(withDuration: 0.5) { () -> Void in
            button.isHidden = false
        }
        
        
    }
    
    //????
    @IBAction func DeleteButtonAction(_ sender: UIButton) {
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            sender.alpha = 0
        }) { (completed: Bool) -> Void in
            sender.isHidden = true
        }
    }
    @IBAction func addImageButtonAction(_ sender: UIButton) {
        let myImage = UIImageView()
        myImage.contentMode = UIViewContentMode.scaleAspectFit //???
        
        self.horisontalStackView.addArrangedSubview(myImage) //додавати вкінець
        
        UIView.animate(withDuration: 0.25) { () -> Void in
            self.horisontalStackView.layoutIfNeeded()
        }
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0)) { () -> Void in
            let string = self.randomURL()
            let url = NSURL(string:string)!
            
            do {
                let data = try NSData(contentsOfURL: url, opions: NSDataReaingOptions.DataReadingUncached)
                let someImage = UIImage(data:data)
                
                dispatch_async(dispatch_get_main_queue(),{ () -> Void in
                    myImage.image = someImage
                })
            } catch {
                print("Error")
            }
        }
    }
  
    @IBAction func removeImageButtonAction(_ sender: UIButton) {
        
        let myImage = self.horisontalStackView.arrangedSubviews.last as UIView?
        
        guard let image = myImage else {
            print("Nil")
            return
        }
        self.horisontalStackView.removeArrangedSubview(image)
        image.removeFromSuperview()
        UIView.animate(withDuration: 0.25) { () -> Void
            self.horisontalStackView.layoutIfNeeded()
        }
    }
    


}

