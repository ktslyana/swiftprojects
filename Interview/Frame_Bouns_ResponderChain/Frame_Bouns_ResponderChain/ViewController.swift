//
//  ViewController.swift
//  Frame_Bouns_ResponderChain
//
//  Created by Vitalii Todorovych on 12/10/19.
//  Copyright © 2019 Vitalii Todorovych. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        rightButton.addTarget(nil, action: #selector(showDetails), for: .touchUpInside)
        
    }

    @objc @IBAction func showDetails(_ sender: Any) {
        print("BaseView frame: \(baseView.frame)")
        print("BaseView bounds: \(baseView.bounds)")
    }
    
    @IBAction func changeBounds(_ sender: Any) {
        var bounds = baseView.bounds
        bounds.origin.x += 10
        bounds.origin.y += 5
        baseView.bounds = bounds
    }
    
    @IBAction func changeFrame(_ sender: Any) {
        var frame = baseView.frame
        frame.origin.x += 10
        frame.origin.y += 5
        baseView.frame = frame
    }
}

