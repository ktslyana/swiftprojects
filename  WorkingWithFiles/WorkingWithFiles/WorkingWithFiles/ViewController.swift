//
//  ViewController.swift
//  WorkingWithFiles
//
//  Created by Vitalii Todorovych on 8/7/19.
//  Copyright © 2019 Correnet. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textFileName: UITextField!
    @IBOutlet weak var textFileNumber: UITextField!
    
    var pathToSave = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.libraryDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0]
    @IBAction func SaveButton(_ sender: Any) {
        
        let didForSave = NSDictionary(objects: [textFileName.text!,textFileNumber.text!], forKeys: ["name" as NSCopying,"number" as NSCopying])
        didForSave.write(toFile: pathToSave+"/data.plist", atomically: true)
    }
    @IBAction func LoabButton(_ sender: Any) {
        pathToSave = pathToSave + "/data.plist"
        
        let dict = NSDictionary(contentsOfFile: pathToSave)
        
        textFileName.text = dict?.object(forKey: "name") as? String
        textFileNumber.text = dict?.object(forKey: "number") as? String
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

