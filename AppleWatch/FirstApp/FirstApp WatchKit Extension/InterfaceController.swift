//
//  InterfaceController.swift
//  FirstApp WatchKit Extension
//
//  Created by Vitalii Todorovych on 11/26/18.
//  Copyright © 2018 Correnet. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    @IBAction func Button() {
        FirstLabel.setText("Hello world")
       
    }
    @IBOutlet var FirstLabel: WKInterfaceLabel!
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
