//
//  ViewController.swift
//  NewsReader
//
//  Created by Yana Kitsul on 7/5/19.
//  Copyright © 2019 Correnet. All rights reserved.
//

import UIKit
import SafariServices

class ViewController: UIViewController {
    @IBOutlet weak var articleTableView: UITableView!
    @IBOutlet weak var loadMoreButton: UIButton!
    private let refreshControl = UIRefreshControl()
    private var articles = [Article]()
    private let dateFormatter = ISO8601DateFormatter()
    
    private let articlesProvider = ArticlesProvider()
    private let imageProvider = ImageProvider()
    
    private var searchText : String? = nil
    private var page: Int = 1
    private let pageSize = 10
    private let sortByKey = "publishedAt"
    
    private var noMoreData = false {
        didSet {
            loadMoreButton.isEnabled = !noMoreData
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        reloadData()
    }

    private func setup() {
        
        //Setup dataSource and delegate for tableview
        articleTableView.dataSource = self
        articleTableView.delegate = self
        
        //Adding UIRefreshControl
        refreshControl.addTarget(self, action: #selector(reloadData), for: .valueChanged)
        articleTableView.refreshControl = refreshControl
    }
    
    @objc private func reloadData() {
        page = 1
        noMoreData = false
        self.articles.removeAll()
        fetchData()
    }
    
    private func fetchData() {
        if let searchText = searchText {
            articlesProvider.fetch(searchText: searchText, sortBy: sortByKey, page: page, pageSize: pageSize, delegate: self)
        } else {
            articlesProvider.fetch(searchText: "", sortBy: sortByKey, page: page, pageSize: pageSize, delegate: self)
        }
    }
    
    @IBAction func loadMoreAction(_ sender: Any) {
        page += 1
        fetchData()
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let theCell = tableView.dequeueReusableCell(withIdentifier: "ArticleCell") ?? ArticleCell()
        if  articles.count > indexPath.row, let theCell = theCell as? ArticleCell {
            let articleObject = articles[indexPath.row]
            theCell.articleTitleLabel.text = articleObject.title ?? ""
            theCell.articleDescriptionLabel.text = articleObject.description ?? ""
            theCell.authorLabel.text = articleObject.author ?? ""
            if let imageUrlString = articleObject.urlToImage, let imageUrl = URL(string: imageUrlString) {
                imageProvider.loadImage(imageUrl: imageUrl, forImageView: theCell.previewImageView)
            }
        }
        return theCell
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let articleObject = articles[indexPath.row]
        if let urlString = articleObject.urlToImage, let url = URL(string: urlString) {
            let webView = SFSafariViewController(url: url)
            self.show(webView, sender: nil)
        }
    }
}

extension ViewController: ProviderResponceble {
    
    func handle(loadedArticles: [Article]?) {
        DispatchQueue.main.async {
            if let loadedArticles = loadedArticles {
                self.articles.append(contentsOf: loadedArticles)
                if loadedArticles.count < self.pageSize {
                    self.noMoreData = true
                }
            } else {
                self.articles.removeAll()
            }
            self.articleTableView.reloadData()
            self.refreshControl.endRefreshing()
        }
    }
    
    func handle(error: String?) {
        print(error?.description ?? "")
    }
}

extension ViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchText = searchBar.text
        reloadData()
        searchBar.endEditing(true)
    }
}

