//
//  ArticlesProvider.swift
//  NewsReader
//
//  Created by Yana Kitsul on 7/5/19.
//  Copyright © 2019 Correnet. All rights reserved.
//

import Foundation

protocol ProviderResponceble {
    func handle(loadedArticles: [Article]?)
    func handle(error: String?)
}

struct ArticlesProvider {
    
    func fetch(searchText: String, sortBy: String, page: Int, pageSize: Int, delegate: ProviderResponceble) {
        
        if let url = URL(string: "https://newsapi.org/v2/top-headlines?country=us&q=\(searchText)&page=\(page)&pageSize=\(pageSize)&sortBy=\(sortBy)&apiKey=99bbde55e27a483ba0cacc8191213312") {
            let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, responce, error) in
                
                if let error = error {
                    delegate.handle(error: error.localizedDescription)
                    return
                }
                if let data = data {
                    if let json = try? JSONSerialization.jsonObject(with: data, options: []) {
                        if let dict = json as? [String: Any] {
                            if let articles = dict["articles"] as? [[String: Any]] {
                                let mappedArticles = articles.compactMap({ (dict) -> Article? in
                                    if let data = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted) {
                                        return try? JSONDecoder().decode(Article.self, from: data)
                                    }
                                    return nil
                                })
                                delegate.handle(loadedArticles: mappedArticles)
                                return
                            }
                        }
                    }
                }
                delegate.handle(error: "Can not parse responce")
            })
            
            task.resume()//Start loading data
        }
    }
    
}
