//
//  ActicleCell.swift
//  NewsReader
//
//  Created by Yana Kitsul on 7/6/19.
//  Copyright © 2019 Correnet. All rights reserved.
//

import UIKit

class ArticleCell : UITableViewCell {
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var articleTitleLabel: UILabel!
    @IBOutlet weak var articleDescriptionLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
}
