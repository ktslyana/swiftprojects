//
//  Article.swift
//  NewsReader
//
//  Created by Yana Kitsul on 9/3/19.
//  Copyright © 2019 Correnet. All rights reserved.
//

import Foundation

struct Article: Codable {
    let title: String?
    let description: String?
    let author: String?
    let urlToImage: String?
    let url: String?
}
