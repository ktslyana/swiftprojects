//
//  ImageProvider.swift
//  News
//
//  Created by Vitalii Todorovych on 8/27/19.
//  Copyright © 2019 Correnet. All rights reserved.
//

import UIKit

struct ImageProvider {
    func loadImage(imageUrl: URL,forImageView: UIImageView) {
        DispatchQueue.main.async {
            forImageView.image = nil
        }
        let downloadTask = URLSession.shared.dataTask(with: imageUrl) { (data, responce, error) in
            if let data = data {
                DispatchQueue.main.async {
                    forImageView.image = UIImage(data: data)
                }
            }
        }
        downloadTask.resume()
    }
}

