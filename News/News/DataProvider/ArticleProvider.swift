//
//  ArticleProvider.swift
//  News
//
//  Created by Vitalii Todorovych on 8/27/19.
//  Copyright © 2019 Correnet. All rights reserved.
//

import Foundation

protocol ProviderResponceble {
    func handle(articles: [[String: Any]]?)
    func handle(error: String?)
}

struct ArticleProvider {
    
    func fetch(searchText: String,delegate: ProviderResponceble) {
        if let url = URL(string: "https://newsapi.org/v2/top-headlines?q=\(searchText)&country=us&apiKey=99bbde55e27a483ba0cacc8191213312") {
            let task = URLSession.shared.dataTask(with: url, completionHandler: { (data,responce,error) in
                if let error = error {
                    delegate.handle(error: error.localizedDescription)
                }
                if let data = data {
                    if let json = try? JSONSerialization.jsonObject(with: data, options: []){
                        if let dict = json as? [String:Any] {
                            if let articles = dict["articles"] as? [[String:Any]] {
                                delegate.handle(articles: articles)
                                return
                            }
                        }
                    }
                }
                delegate.handle(error: "Can not parse")
            })
            task.resume()
        }
    }
}
 
