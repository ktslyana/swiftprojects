//
//  ViewController.swift
//  News
//
//  Created by Vitalii Todorovych on 8/27/19.
//  Copyright © 2019 Correnet. All rights reserved.
//

import UIKit
import SafariServices

class ViewController: UIViewController {

    @IBOutlet weak var ArticleTableView: UITableView!
    
  
    
    var articles: [[String:Any]]?
    
    let articleProvider = ArticleProvider()
    let imageProvider = ImageProvider()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        articleProvider.fetch(searchText: "", delegate: self as! ProviderResponceble)
        
        ArticleTableView.dataSource = self
        ArticleTableView.delegate = self as! UITableViewDelegate 
        // Do any additional setup after loading the view, typically from a nib.
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshAction), for: .valueChanged)
        ArticleTableView.refreshControl = refreshControl
        
        
    /*    refreshControl = UIRefreshControl()
         refreshControl.addTarget(self, action: "refresh:", for: UIControlEvents.valueChanged)
*/
        
    }

    @objc func refreshAction()  {
        ArticleTableView.reloadData()
        let webView = WebViewController(nibName: "WebViewController", bundle: nil)
        present(webView, animated: true) {

        }
//        let urlString = "https://www.hackingwithswift.com"
//
//        if let url = URL(string: urlString) {
//            let vc = SFSafariViewController(url: url, entersReaderIfAvailable: true)
////            vc.delegate = self
//
//            present(vc, animated: true)
//        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
    
    extension ViewController: UITableViewDataSource {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if let articles = self.articles {
                return articles.count
            } else {
                return 0
            }
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let theCell = tableView.dequeueReusableCell(withIdentifier: "ArticleCell") ?? ArticleCell()
            if  let articles = articles, let theCell = theCell as? ArticleCell {
                let articleObject = articles[indexPath.row]
                if let articleTitle =  articleObject["title"] as? String {
                    theCell.articleTitleLabel.text = articleTitle
                }
                if let articleDescription =  articleObject["description"] as? String {
                    theCell.articleDescriptionLabel.text = articleDescription
              }
               if let articleimageUrl =  articleObject["urlToImage"] as? String, let imageUrl = URL(string: articleimageUrl) {
                imageProvider.loadImage(imageUrl: imageUrl, forImageView: theCell.articleImage)
             }
               if let articleAuthor =  articleObject["author"] as? String {
                 theCell.articleAuthorLabel.text = articleAuthor
              }
             if let articleSource = articleObject["name"] as? String {
                 theCell.articleSourceLabel.text = articleSource
              }
            
            }
            return theCell
        }
}

extension ViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let enterdText = searchBar.text {
            articleProvider.fetch(searchText: enterdText, delegate: self as! ProviderResponceble)
        } else {
            articleProvider.fetch(searchText: "", delegate: self as! ProviderResponceble)
        }
        searchBar.endEditing(true)
    }
}



