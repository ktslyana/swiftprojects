//
//  ArticleCell.swift
//  News
//
//  Created by Vitalii Todorovych on 8/27/19.
//  Copyright © 2019 Correnet. All rights reserved.
//

import UIKit

class ArticleCell: UITableViewCell {

  

 
    @IBOutlet weak var articleTitleLabel: UILabel!
    @IBOutlet weak var articleImage: UIImageView!
    
    @IBOutlet weak var articleDescriptionLabel: UILabel!
    @IBOutlet weak var articleAuthorLabel: UILabel!
    
    @IBOutlet weak var articleSourceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
