//
//  Model.swift
//  VisitCards
//
//  Created by YanaKitsul on 8/8/19.
//  Copyright © 2019 Correnet. All rights reserved.
//

import UIKit

var pathForLibrary: String {
     let path = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.libraryDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0]
    return path
}
var pathForData: String {
    
    return path + "/dataArray.plist"
    
}

class Model: NSObject {
    class func loadData() {
        dataArray = []
        let array = NSArray(contentsOfFile: pathForData)
        if let array = array {
            for dict in array {
                dataArray.append(VisitCard(dictionary: dict as! NSDictionary))
            }
        }
    }
     class func getNextId() -> Int {
            var nextId = 0
            for vc in dataArray {
                if vc.id >= nextId {
                    nextId = vc.id + 1
                }
            }
            return nextId
        }
    
    class func saveData() {
        var arrayForSave = NSArray()
        for visitcard in dataArray {
            arrayForSave = arrayForSave.ArrayByAddingObject(visitcard.getDictionaryForSave())
        }
        arrayForSave.write(toFile: pathForData, atomically: true)
    }
}

var dataArray : [VisitCard] = []

class VisitCard {
   
    var id : Int
    var name : String
    var email : String
    var phone : String
    var imagePath : String {
        
        return pathForLibrary + "\(id).jpg"
        return ""
    }
    
    var image : UIImage? {
        get {
           return UIImage(contentsOfFile: imagePath)
        }
        set {
            if newValue == nil {
                let _ = try? FileManager.defaultManager().removeItemPath(imagePath)
            } else {
                UIImageJPEGRepresentation(newValue!, 1.0).writeToFile(imagePath,atomically:true)
            }
        }
    }
    
    init() {
        self.id = Model.getNextId()
        self.name = ""
        self.email = ""
        self.phone = ""
        
    }
    init(name:String,email:String,phone:String) {
        self.id = Model.getNextId()
        self.name = name
        self.email = email
        self.phone = phone
     
    }
    init(dictionary:NSDictionary) {
        self.id = dictionary.object(forKey: "id") as! Int
        self.name = dictionary.object(forKey: "name") as! String
        self.email = dictionary.object(forKey: "email") as! String
        self.phone = dictionary.object(forKey: "phone") as! String
       // self.imageName = dictionary.object(forKey: "imageName") as! String
        
    }
    func getDictionaryForSave() -> NSDictionary {
        
        let dict = NSDictionary(objects: [id,name,email,phone], forKeys: ["id" as NSCopying, "name" as NSCopying,"email" as NSCopying,"phone" as NSCopying])
        return dict
    }
}
