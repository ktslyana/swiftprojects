//
//  OneCardController.swift
//  VisitCards
//
//  Created by Vitalii Todorovych on 8/26/19.
//  Copyright © 2019 Correnet. All rights reserved.
//

import UIKit

class OneCardController: UITableViewController {
    
    var card : VisitCard
    
    
    @IBAction func callButton(_ sender: Any) {
        let str = "tel://\(textFieldPhone.text!)"
        let url : NSURL(String:str)
        
        if url != nil { UIApplication.shared.openURL(url as URL)
        } else {
            return print("Error url")
        }
    }
    
 
    
    @IBAction func sendEmailButton(_ sender: Any) {
        
        
    }
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var texFieldName: UITextField!
    @IBOutlet weak var textFieldPhone: UITextField!
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBAction func saveButton(_ sender: Any) {
        if let card = card {
        card.name =  texFieldName.text!
        card.phone =  textFieldPhone.text!
        card.email = textFieldEmail.text!
        card.image = imageView.image
        Model.saveData()
        } else {
           let newCard = VisitCard(name: texFieldName.text!, email: textFieldEmail.text!, phone: textFieldPhone.text!)
            dataArray.insert(newCard, at: 0)
            
            newCard.image = imageView.image
            
            Model.saveData()
        }
        navigationController?.popToRootViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        if let card = card {
            texFieldName.text = card.name
            textFieldPhone.text = card.phone
            textFieldEmail.text = card.email
            
            imageView.image = card.image
        }
    }
    
    let imagePicker = UIImagePickerController()
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            
            present(imagePicker, animated: true, completion: nil)
        }
    }
}


extension OneCardController: UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        imageView.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        picker.dismiss(animated: true, completion: nil)

    }
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}




























